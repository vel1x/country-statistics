<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Country;
use AppBundle\Service\StatisticsService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Country controller.
 *
 * @Route("country")
 */
class CountryController extends Controller
{
    /**
     * Lists all country entities.
     *
     * @Route("/", name="country_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $countries = $em->getRepository('AppBundle:Country')->findAll();

        return $this->render('country/index.html.twig', array(
            'countries' => $countries,
        ));
    }

    /**
     * Creates a new country entity.
     *
     * @Route("/new", name="country_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $country = new Country();
        $form = $this->createForm('AppBundle\Form\CountryType', $country);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($country);
            $em->flush();

            return $this->redirectToRoute('country_show', array('id' => $country->getId()));
        }

        return $this->render('country/new.html.twig', array(
            'country' => $country,
            'form'    => $form->createView(),
        ));
    }

    /**
     * Finds and displays a country entity.
     *
     * @Route("/{id}", name="country_show")
     * @Method("GET")
     */
    public function showAction(Country $country)
    {
        return $this->render('country/show.html.twig', array(
            'country'        => $country,
            'avgTaxRate'     => StatisticsService::computeCountryAverageTaxRate($country),
            'collectedTaxes' => StatisticsService::computeCountryOverallCollected($country),
        ));
    }

    /**
     * Displays a form to edit an existing country entity.
     *
     * @Route("/{id}/edit", name="country_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Country $country)
    {
        $editForm = $this->createForm('AppBundle\Form\CountryType', $country);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('country_edit', array('id' => $country->getId()));
        }

        return $this->render('country/edit.html.twig', array(
            'country'     => $country,
            'edit_form'   => $editForm->createView(),
        ));
    }

    /**
     * Deletes a country entity.
     *
     * @Route("country/delete/{id}", name="country_delete")
     */
    public function deleteAction(Country $country)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($country);
        $em->flush();

        return $this->redirectToRoute('country_index');
    }
}
