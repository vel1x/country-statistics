<?php

namespace AppBundle\Controller;

use AppBundle\Entity\County;
use AppBundle\Entity\State;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * County controller.
 */
class CountyController extends Controller
{
    /**
     * Creates a new county entity.
     *
     * @Route("state/{id}/county/new", name="county_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, State $state)
    {
        $county = new County();
        $county->setState($state);
        $form = $this->createForm('AppBundle\Form\CountyType', $county);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($county);
            $em->flush();

            return $this->redirectToRoute('state_show', array('id' => $state->getCountry()->getId(), 'state_id' => $state->getId()));
        }

        return $this->render('county/new.html.twig', array(
            'county' => $county,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing county entity.
     *
     * @Route("state/{id}/county/{county_id}/edit", name="county_edit")
     * @Entity("county", expr="repository.find(county_id)")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, State $state, County $county)
    {
        $editForm = $this->createForm('AppBundle\Form\CountyType', $county);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('state_show', array('id' => $state->getCountry()->getId(), 'state_id' => $state->getId()));
        }

        return $this->render('county/edit.html.twig', array(
            'state'       => $state,
            'county'      => $county,
            'edit_form'   => $editForm->createView(),
        ));
    }

    /**
     * Deletes a county entity.
     *
     * @Route("county/delete/{id}", name="county_delete")
     */
    public function deleteAction(County $county)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($county);
        $em->flush();

        return $this->redirectToRoute(
            'state_show',
            [
                'id'       => $county->getState()->getCountry()->getId(),
                'state_id' => $county->getState()->getId(),
            ]
        );
    }
}
