<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Country;
use AppBundle\Entity\State;
use AppBundle\Service\StatisticsService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * State controller.
 */
class StateController extends Controller
{
    /**
     * Lists all state entities.
     *
     * @Route("country/{id}/states/", name="state_index")
     * @Method("GET")
     */
    public function indexAction(Country $country)
    {
        $em = $this->getDoctrine()->getManager();

        $states = $em->getRepository('AppBundle:State')->findBy(
            ['country' => $country]
        );

        return $this->render('state/index.html.twig', array(
            'states'  => $states,
            'country' => $country,
        ));
    }

    /**
     * Creates a new state entity.
     *
     * @Route("country/{id}/state/new", name="state_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, Country $country)
    {
        $state = new State();
        $state->setCountry($country);
        $form = $this->createForm('AppBundle\Form\StateType', $state);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($state);
            $em->flush();

            return $this->redirectToRoute('country_show', array('id' => $country->getId()));
        }

        return $this->render('state/new.html.twig', array(
            'state' => $state,
            'form'  => $form->createView(),
        ));
    }

    /**
     * Finds and displays a state entity.
     *
     * @Route("/country/{id}/state/{state_id}", name="state_show")
     * @Entity("state", expr="repository.find(state_id)")
     * @Method("GET")
     */
    public function showAction(Country $country, State $state)
    {
        return $this->render('state/show.html.twig', array(
            'state'          => $state,
            'avgTaxRate'     => StatisticsService::computeStateAverageTaxRate($state),
            'collectedTaxes' => StatisticsService::computeStateOverallCollected($state),
        ));
    }

    /**
     * Displays a form to edit an existing state entity.
     *
     * @Route("/country/{id}/state/{state_id}/edit", name="state_edit")
     * @Entity("state", expr="repository.find(state_id)")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Country $country, State $state)
    {
        $editForm = $this->createForm('AppBundle\Form\StateType', $state);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('country_show', array('id' => $country->getId()));
        }

        return $this->render('state/edit.html.twig', array(
            'state'     => $state,
            'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a state entity.
     *
     * @Route("state/delete/{id}", name="state_delete")
     */
    public function deleteAction(State $state)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($state);
        $em->flush();

        return $this->redirectToRoute('country_show', ['id' => $state->getCountry()->getId()]);
    }
}
