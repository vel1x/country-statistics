<?php

namespace AppBundle\Extension\Twig;

use AppBundle\Entity\Country;
use AppBundle\Entity\State;
use AppBundle\Entity\StatisticsInterface;
use AppBundle\Service\StatisticsService;

class StatisticsExtension extends \Twig_Extension
{
    public function getFilters(): array
    {
        return array(
            new \Twig_SimpleFilter("avgTax", array($this, "avgTaxFilter")),
            new \Twig_SimpleFilter("collected", array($this, "collectedTaxesFilter")),
        );
    }

    public function collectedTaxesFilter(StatisticsInterface $object): float
    {
        $collected = 0;
        switch (true) {
            case $object instanceOf State:
                $collected = StatisticsService::computeStateOverallCollected($object);
                break;
            case $object instanceOf Country:
                $collected = StatisticsService::computeCountryOverallCollected($object);
                break;
        }

        return $collected;
    }

    public function avgTaxFilter(StatisticsInterface $object): float
    {
        $avgTax = 0;
        switch (true) {
            case $object instanceOf State:
                $avgTax = StatisticsService::computeStateAverageTaxRate($object);
                break;
            case $object instanceOf Country:
                $avgTax = StatisticsService::computeCountryAverageTaxRate($object);
                break;
        }

        return $avgTax;
    }

    public function getName(): string
    {
        return "statistics_twig_extension";
    }
}
