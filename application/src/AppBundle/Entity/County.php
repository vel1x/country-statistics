<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * County
 * @UniqueEntity(fields={"name", "state"})
 * @ORM\Table(name="county")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CountyRepository")
 */
class County
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, unique=true)
     */
    private $name;

    /**
     * @var float
     *
     * @ORM\Column(name="taxRate", type="float")
     */
    private $taxRate;

    /**
     * @var float
     *
     * @ORM\Column(name="taxesAmount", type="float")
     */
    private $taxesAmount;

    /**
     * @var State
     * @ORM\ManyToOne(targetEntity="State", inversedBy="counties")
     * @ORM\JoinColumn(name="state_id", referencedColumnName="id")
     */
    private $state;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getTaxRate(): ?float
    {
        return $this->taxRate;
    }

    public function setTaxRate(float $taxRate): void
    {
        $this->taxRate = $taxRate;
    }

    public function getTaxesAmount(): ?float
    {
        return $this->taxesAmount;
    }

    public function setTaxesAmount(float $taxesAmount): void
    {
        $this->taxesAmount = $taxesAmount;
    }

    public function getState(): State
    {
        return $this->state;
    }

    public function setState(State $state): void
    {
        $this->state = $state;
    }
}
