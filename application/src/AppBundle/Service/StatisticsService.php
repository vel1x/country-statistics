<?php

namespace AppBundle\Service;


use AppBundle\Entity\Country;
use AppBundle\Entity\County;
use AppBundle\Entity\State;

class StatisticsService
{
    public static function computeStateAverageTaxRate(State $state): float
    {
        $taxRate = 0;

        /** @var County $county */
        foreach ($state->getCounties() as $county) {
            $taxRate += $county->getTaxRate();
        }

        return $taxRate > 0 ? round($taxRate/$state->getCounties()->count(), 2) : $taxRate;
    }

    public static function computeStateOverallCollected(State $state): float
    {
        $collected = 0;
        /** @var County $county */
        foreach ($state->getCounties() as $county) {
            $collected += $county->getTaxesAmount();
        }

        return $collected;
    }

    public static function computeCountryAverageTaxRate(Country $country): float
    {
        $taxRate = 0;
        /** @var State $state */
        foreach ($country->getStates() as $state) {
            $taxRate += static::computeStateAverageTaxRate($state);
        }

        return $taxRate != 0 ? round($taxRate/$country->getStates()->count(), 2) : $taxRate;
    }

    public static function computeCountryOverallCollected(Country $country): float
    {
        $collected = 0;
        foreach ($country->getStates() as $state) {
            $collected += static::computeStateOverallCollected($state);
        }

        return $collected;
    }
}
