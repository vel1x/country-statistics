<?php

public class StringTools
{
    public static function concat(string $a, string $b)
    {
        return  $a . $b;
    }

    /**
     * @param array $array - Array of string elements
     *
     * @return string
     */
    public static function concatenate(array $array): string
    {
        return implode("", $array);
    }

    public static function toUpperCase(string $string): string
    {
        return strtoupper($string);
    }

    public static function toLowerCase(string $input): string
    {
        return strtolower($input);
    }

    /* Generate a hash value */
    public static function hash(string $algorithm, string $input): string
    {
        return hash($algorithm, $input);
    }

    public static function replace(string $search, string $replace, string $subject): string
    {
        return str_replace($search, $replace, $subject);
    }

    public static function trim(string $input): string
    {
        return trim($input);
    }
}
